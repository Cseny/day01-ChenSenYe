# O

- Code review.
- TDD exercise.


# R

This is a fruitful day.

# I

- For the first time sharing screens for code review, I corrected many code specification issues with the help of my classmates.
- In the code review, I learned the implementation method of Strategy pattern from other students' code.

# D

Through the practice of combining TDD with OO, we have further deepened our understanding of TDD. In complex case studies, TDD can enhance the comprehensiveness of our conceptualized code and make the resulting code more robust.