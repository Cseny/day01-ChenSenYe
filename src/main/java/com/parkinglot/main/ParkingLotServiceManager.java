package com.parkinglot.main;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.*;

public class ParkingLotServiceManager extends ParkingBoy {
    List<ParkingBoy> parkingBoys;

    public ParkingLotServiceManager(List<ParkingLot> parkingLots) {
        super(parkingLots);
        parkingBoys = new ArrayList<>();
    }


    public void addParkingBoy(ParkingBoy parkingBoy) {
        parkingBoys.add(parkingBoy);
    }

    public ParkingTicket specifyPark(int boyNo, Car car) {
        return Optional.ofNullable(parkingBoys.get(boyNo))
                .map(parkingBoy -> parkingBoy.park(car))
                .orElseThrow();
    }

    public Car specifyFetch(ParkingTicket parkingTicket) throws UnrecognizedParkingTicketException {
        return parkingBoys.stream()
                .map(parkingBoy -> parkingBoy.fetch(parkingTicket))
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow();
    }
}
