package com.parkinglot.main;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<ParkingTicket, Car> map;
    private final int capacity;

    public ParkingLot(int capacity) {
        map = new HashMap<>();
        this.capacity = capacity;
    }

    public ParkingTicket park(Car car) throws NoAvailablePositionException {
        if (isFull()) {
            throw new NoAvailablePositionException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        map.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) throws UnrecognizedParkingTicketException {
        if (!isValidTicket(parkingTicket)) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        return map.remove(parkingTicket);
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean isFull() {
        return map.size() >= capacity;
    }

    public boolean isValidTicket(ParkingTicket parkingTicket) {
        Car car = map.get(parkingTicket);
        return car != null;
    }

    public int getEmptyPositions() {
        return capacity - map.size();
    }

    public double getAvailablePositionRate() {
        return (double) (capacity - map.size()) / (double) capacity;
    }
}
