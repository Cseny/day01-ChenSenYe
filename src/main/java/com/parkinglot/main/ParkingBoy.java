package com.parkinglot.main;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;

import java.util.List;

public class ParkingBoy {
    List<ParkingLot> parkingLots;

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingTicket park(Car car) throws NoAvailablePositionException{
        for (ParkingLot parkingLot : parkingLots) {
            if (!parkingLot.isFull()) {
                return parkingLot.park(car);
            }
        }
        throw new NoAvailablePositionException("No available position");
    }

    public Car fetch(ParkingTicket parkingTicket) throws UnrecognizedParkingTicketException{
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isValidTicket(parkingTicket)) {
                return parkingLot.fetch(parkingTicket);
            }
        }
        throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
    }
}
