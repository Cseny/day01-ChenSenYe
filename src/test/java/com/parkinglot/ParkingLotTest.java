package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;
import com.parkinglot.main.Car;
import com.parkinglot.main.ParkingLot;
import com.parkinglot.main.ParkingTicket;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_car() {
        //Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();

        //When
        ParkingTicket parkingTicket = parkingLot.park(car);

        //Then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_two_different_ticket_when_park_given_two_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);

        assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_return_correct_car_when_fetch_given_a_valid_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);

        Car fetchedCar = parkingLot.fetch(parkingTicket);

        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_null_when_fetch_given_a_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        parkingLot.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingLot.fetch(wrongTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_given_without_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        parkingLot.park(car);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingLot.fetch(null);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_given_a_car_and_parkinglot_full() {
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            Car car = new Car();
            parkingLot.park(car);
        }
        Car car = new Car();

        NoAvailablePositionException exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> {
            parkingLot.park(car);
        });
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_given_a_used_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        parkingLot.fetch(parkingTicket);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingLot.fetch(parkingTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
}
