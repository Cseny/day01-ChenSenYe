package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;
import com.parkinglot.main.Car;
import com.parkinglot.main.ParkingLot;
import com.parkinglot.main.ParkingTicket;
import com.parkinglot.main.SuperParkingBoy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SuperParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_by_super_parking_boy_given_a_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car = new Car();

        ParkingTicket parkingTicket = superParkingBoy.park(car);

        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_two_different_ticket_when_park_by_super_parking_boy_given_two_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingTicket ticket1 = superParkingBoy.park(car1);
        ParkingTicket ticket2 = superParkingBoy.park(car2);

        assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_return_correct_car_when_fetch_by_super_parking_boy_given_a_valid_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car = new Car();
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        Car fetchedCar = superParkingBoy.fetch(parkingTicket);

        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_null_when_fetch_by_super_parking_boy_given_a_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car = new Car();
        superParkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            superParkingBoy.fetch(wrongTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_by_super_parking_boy_given_without_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car = new Car();
        superParkingBoy.park(car);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            superParkingBoy.fetch(null);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_super_parking_boy_given_a_car_and_parkinglot_full() {
        ParkingLot parkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        superParkingBoy.park(car1);
        Car car2 = new Car();

        NoAvailablePositionException exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> {
            superParkingBoy.park(car2);
        });
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_by_super_parking_boy_given_a_used_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car = new Car();
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        superParkingBoy.fetch(parkingTicket);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            superParkingBoy.fetch(parkingTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_right_car_when_park_by_super_parking_boy_given_two_car() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot1.park(car1);
        ParkingTicket parkingTicket2 = parkingLot2.park(car2);

        Car fetchedCar1 = superParkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = superParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_unrecognized_parking_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot1.park(car1);
        parkingLot2.park(car2);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            superParkingBoy.fetch(new ParkingTicket());
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_used_parking_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket = parkingLot1.park(car1);
        parkingLot2.park(car2);
        parkingLot1.fetch(parkingTicket);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            superParkingBoy.fetch(parkingTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_car_and_without_any_position() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLots);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());

        NoAvailablePositionException exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> {
            superParkingBoy.park(new Car());
        });
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_park_larger_available_position_rate_parkinglot_when_park_by_super_parking_boy_given_a_car_and_two_parkinglot() {
        ParkingLot parkingLot1 = new ParkingLot(3);
        ParkingLot parkingLot2 = new ParkingLot(2);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperParkingBoy SuperParkingBoy = new SuperParkingBoy(parkingLots);
        parkingLot1.park(new Car());
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        Car car = new Car();

        ParkingTicket parkingTicket = SuperParkingBoy.park(car);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }
}
