package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedParkingTicketException;
import com.parkinglot.main.Car;
import com.parkinglot.main.ParkingBoy;
import com.parkinglot.main.ParkingLot;
import com.parkinglot.main.ParkingTicket;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_by_parking_boy_given_a_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();

        ParkingTicket parkingTicket = parkingBoy.park(car);

        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_two_different_ticket_when_park_by_parking_boy_given_two_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);

        assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_return_correct_car_when_fetch_by_parking_boy_given_a_valid_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);

        Car fetchedCar = parkingBoy.fetch(parkingTicket);

        assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_null_when_fetch_by_parking_boy_given_a_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        parkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingBoy.fetch(wrongTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_by_parking_boy_given_without_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        parkingBoy.park(car);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingBoy.fetch(null);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_parking_boy_given_a_car_and_parkinglot_full() {
        ParkingLot parkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        parkingBoy.park(car1);
        Car car2 = new Car();

        NoAvailablePositionException exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(car2);
        });
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_null_when_fetch_by_parking_boy_given_a_used_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        parkingBoy.fetch(parkingTicket);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingBoy.fetch(parkingTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_park_first_parkinglot_when_park_by_parking_boy_given_a_car_and_two_parkinglot() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();

        ParkingTicket parkingTicket = parkingBoy.park(car);
        Car fetchedCar = parkingLot1.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_park_second_parkinglot_when_park_by_parking_boy_given_a_car_and_first_parkinglot_full() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();

        parkingBoy.park(car1);
        ParkingTicket parkingTicket = parkingBoy.park(car2);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(car2, fetchedCar);
    }

    @Test
    void should_return_right_car_when_park_by_parking_boy_given_two_car() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot1.park(car1);
        ParkingTicket parkingTicket2 = parkingLot2.park(car2);

        Car fetchedCar1 = parkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = parkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_unrecognized_parking_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        parkingLot1.park(car1);
        parkingLot2.park(car2);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingBoy.fetch(new ParkingTicket());
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_used_parking_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket = parkingLot1.park(car1);
        parkingLot2.park(car2);
        parkingLot1.fetch(parkingTicket);

        UnrecognizedParkingTicketException exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> {
            parkingBoy.fetch(parkingTicket);
        });
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_car_and_without_any_position() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());

        NoAvailablePositionException exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(new Car());
        });
        Assertions.assertEquals("No available position", exception.getMessage());
    }
}
