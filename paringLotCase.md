# ParkingLotCase

**case1: should_return_a_parking_ticket_when_park_given_a_car**

input: Car

output: ParkingTicket

**case2: should_return_two_different_ticket_when_park_given_two_car**

input: Car1, Car2

output: ParkingTicket1, ParkingTicket2

**case3: should_return_correct_car_when_fetch_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case4: should_return_null_when_fetch_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case5: should_return_null_when_fetch_given_without_ticket**

input: null

output: UnrecognizedParkingTicketException

**case6: should_return_null_when_park_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case7: should_return_null_when_fetch_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException



# ParkingBoyCase

**case1: should_return_a_parking_ticket_when_park_by_parking_boy_given_a_car**

input: Car

output: ParkingTicket

**case2: should_return_two_different_ticket_when_park_by_parking_boy_given_two_car**

input: Car1, Car2

output: ParkingTicket1, ParkingTicket2

**case3: should_return_correct_car_when_fetch_by_parking_boy_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case4: should_return_null_when_fetch_by_parking_boy_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case5: should_return_null_when_fetch_by_parking_boy_given_without_ticket**

input: null

output: UnrecognizedParkingTicketException

**case6: should_return_null_when_park_by_parking_boy_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case7: should_return_null_when_fetch_by_parking_boy_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case8: should_return_park_first_parkinglot_when_park_by_parking_boy_given_a_car_and_two_parkinglot**

input: Car

output: Car

**case9: should_return_park_second_parkinglot_when_park_by_parking_boy_given_a_car_and_first_parkinglot_full**

input: Car

output: Car

**case10: should_return_right_car_when_park_by_parking_boy_given_two_car**

input: ParkingTicket1, ParkingTicket2

input: Car1, Car2

**case11: should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_unrecognized_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case12: should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_used_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case13: should_return_null_when_park_by_parking_boy_given_two_parkinglot_and_a_car_and_without_any_position**

input: Car

output: NoAvailablePositionException



# SmartParkingBoyCase

**case1: should_return_a_parking_ticket_when_park_by_smart_parking_boy_given_a_car**

input: Car

output: ParkingTicket

**case2: should_return_two_different_ticket_when_park_by_smart_parking_boy_given_two_car**

input: Car1, Car2

output: ParkingTicket1, ParkingTicket2

**case3: should_return_correct_car_when_fetch_by_smart_parking_boy_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case4: should_return_null_when_fetch_by_smart_parking_boy_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case5: should_return_null_when_fetch_by_smart_parking_boy_given_without_ticket**

input: null

output: UnrecognizedParkingTicketException

**case6: should_return_null_when_park_by_smart_parking_boy_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case7: should_return_null_when_fetch_by_smart_parking_boy_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case8: should_return_park_more_empty_positions_parkinglot_when_park_by_smart_parking_boy_given_a_car_and_two_parkinglot**

input: Car

output: Car

**case9: should_return_right_car_when_park_by_smart_parking_boy_given_two_car**

input: ParkingTicket1, ParkingTicket2

input: Car1, Car2

**case10: should_return_null_when_park_by_smart_parking_boy_given_two_parkinglot_and_a_unrecognized_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case11: should_return_null_when_park_by_smart_parking_boy_given_two_parkinglot_and_a_used_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case12: should_return_null_when_park_by_smart_parking_boy_given_two_parkinglot_and_a_car_and_without_any_position**

input: Car

output: NoAvailablePositionException



# SuperParkingBoyCase

**case1: should_return_a_parking_ticket_when_park_by_super_parking_boy_given_a_car**

input: Car

output: ParkingTicket

**case2: should_return_two_different_ticket_when_park_by_super_parking_boy_given_two_car**

input: Car1, Car2

output: ParkingTicket1, ParkingTicket2

**case3: should_return_correct_car_when_fetch_by_super_parking_boy_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case4: should_return_null_when_fetch_by_super_parking_boy_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case5: should_return_null_when_fetch_by_super_parking_boy_given_without_ticket**

input: null

output: UnrecognizedParkingTicketException

**case6: should_return_null_when_park_by_super_parking_boy_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case7: should_return_null_when_fetch_by_super_parking_boy_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case8: should_return_park_larger_available_position_rate_parkinglot_when_park_by_super_parking_boy_given_a_car_and_two_parkinglot**

input: Car

output: Car

**case9: should_return_right_car_when_park_by_super_parking_boy_given_two_car**

input: ParkingTicket1, ParkingTicket2

input: Car1, Car2

**case10: should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_unrecognized_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case11: should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_used_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case12: should_return_null_when_park_by_super_parking_boy_given_two_parkinglot_and_a_car_and_without_any_position**

input: Car

output: NoAvailablePositionException



# ParkingLotServiceManager

**case1: should_return_a_parking_ticket_when_park_by_parkinglot_service_manager_given_a_car**

input: Car

output: ParkingTicket

**case2: should_return_two_different_ticket_when_park_by_parkinglot_service_manager_given_two_car**

input: Car1, Car2

output: ParkingTicket1, ParkingTicket2

**case3: should_return_correct_car_when_fetch_by_parkinglot_service_manager_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case4: should_return_null_when_fetch_by_parkinglot_service_manager_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case5: should_return_null_when_fetch_by_parkinglot_service_manager_given_without_ticket**

input: null

output: UnrecognizedParkingTicketException

**case6: should_return_null_when_park_by_parkinglot_service_manager_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case7: should_return_null_when_fetch_by_parkinglot_service_manager_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case8: should_return_park_first_parkinglot_when_park_by_parkinglot_service_manager_given_a_car_and_two_parkinglot**

input: Car

output: Car

**case9: should_return_park_second_parkinglot_when_park_by_parkinglot_service_manager_given_a_car_and_first_parkinglot_full**

input: Car

output: Car

**case10: should_return_right_car_when_park_by_parkinglot_service_manager_given_two_car**

input: ParkingTicket1, ParkingTicket2

input: Car1, Car2

**case11: should_return_null_when_park_by_parkinglot_service_manager_given_two_parkinglot_and_a_unrecognized_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case12: should_return_null_when_park_by_parkinglot_service_manager_given_two_parkinglot_and_a_used_parking_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case13: should_return_null_when_park_by_parkinglot_service_manager_given_two_parkinglot_and_a_car_and_without_any_position**

input: Car

output: NoAvailablePositionException

**case14: should_return_a_parking_ticket_when_specify_a_parking_boy_given_a_car**

input: Car

output: ParkingTicket

**case15: should_return_correct_car_when_specify_a_parking_boy_given_a_valid_ticket**

input: ParkingTicket

output: Car

**case16: should_return_null_when_specify_a_parking_boy_given_a_wrong_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException

**case17: should_return_null_when_specify_a_parking_boy_given_a_car_and_parkinglot_full**

input: Car

output: NoAvailablePositionException

**case18: should_return_null_when_specify_a_parking_boy_given_a_used_ticket**

input: ParkingTicket

output: UnrecognizedParkingTicketException